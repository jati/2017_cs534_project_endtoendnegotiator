mkdir -p /tmp/
while true
do
    sox -d /tmp/recording.wav trim 0 5 &> /dev/null
    sox /tmp/recording.wav -c 1 -r 8000 /tmp/recording2.wav &> /dev/null
    curl -T /tmp/recording2.wav  "http://scuba@pano-03.usc.edu:8888/client/dynamic/recognize" -o /tmp/output.txt &> /dev/null
    cat /tmp/output.txt | cut -d'"' -f8
done

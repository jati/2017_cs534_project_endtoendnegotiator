#!/bin/bash

tmpdir=$1
asr_log=../end-to-end-negotiator/src/asr_log/
mkdir -p $asr_log

#sox -d $tmpdir/recording.wav -V1 rate 8k channels 1 silence 1 0.1 3% 0 2.0 3%
#key press
read -n1 -r -p "Press any key to start recording your speech. Recording will stop automatically after detecting 3 seconds of silence..." key
printf '\n\nRecording...\t'
#sox -dq $tmpdir/recording.wav -V1 silence 0 0 2.0 3%
rec -q $tmpdir/recording.wav silence 1 5 0.1% 1 0:3.0 0.1%
printf 'Processing...\n'
sox $tmpdir/recording.wav -r 16k -b 16 $tmpdir/recording2.wav

curl -T $tmpdir/recording2.wav  "http://scuba@pano-03.usc.edu:8888/client/dynamic/recognize" -o $tmpdir/kaldi_output.tmp 2> $tmpdir/log.txt
#curl -T $tmpdir/recording.wav  "http://scuba@pano-03.usc.edu:8888/client/dynamic/recognize" -o $tmpdir/kaldi_output.tmp
#curl -T $tmpdir/recording2.wav  "http://scuba@pano-03.usc.edu:8888/client/dynamic/recognize" -o $tmpdir/kaldi_output.tmp
cat $tmpdir/kaldi_output.tmp | cut -d'"' -f8 > $tmpdir/kaldi_output.tmp2

#mv $tmpdir/kaldi_output.tmp2 $tmpdir/output.txt
cat $tmpdir/kaldi_output.tmp2 | sed 's/\[[^][]*\]//g' | sed 's/<unk>//g' | tr -d '[:punct:]' > $tmpdir/output.txt
cat $tmpdir/output.txt >> $asr_log/asr_log.txt

#printf "ASR output:\t"
#cat $tmpdir/output.txt

rm $tmpdir/kaldi_output.tmp $tmpdir/kaldi_output.tmp2 2>/dev/null

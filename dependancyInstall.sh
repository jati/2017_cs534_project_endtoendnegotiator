#cd -
pip3.5 install --upgrade pip
brew install sox
# pip3.5 install torch
pip3 install http://download.pytorch.org/whl/torch-0.2.0.post3-cp35-cp35m-macosx_10_7_x86_64.whl
pip3 install torchvision
pip3 install visdom
pip install --upgrade watson-developer-cloud
pip3 install selenium
brew install chromedriver

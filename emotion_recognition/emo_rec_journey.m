y = [58.46, 37.93, 51.75, 58.97, 59.38];
x = 1:5;
txt = {'State-of-the-art (4 emotions) [3]', 'Try 1: 6 emotions (4+excited+frustrated)', 'Try 2: 4 emotions',...
    'Try 3: Balanced class weights', 'Try 4: Train on IEMOCAP+emodb, test on IEMOCAP'};

plot(x(1),y(1), 'sb', 'MarkerSize', 12);
hold on;
plot(x(2:end), y(2:end), 'r*-', 'MarkerSize', 12)
axis([0 6 30 70])
for i=1:5
    txt_to_add = [txt{i} ' [' num2str(y(i)) '%] ']
    text(x(i), y(i), txt_to_add, 'HorizontalAlignment','right', 'FontSize', 12, 'FontWeight', 'bold');
end
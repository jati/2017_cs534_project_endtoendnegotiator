import pdb, os, sys, glob
import numpy as np

audio_dir='/Volumes/WD_external_2TB/datasets/emodb/wav/'


tmpdir='/tmp/emodb_process_tmp/'
os.system('mkdir -p '+tmpdir)
os.system('rm -r '+tmpdir+'*')

audio_files = glob.glob(audio_dir+'/*.wav')
for audio_file in audio_files:
    audio_filename = audio_file.split('/')[-1].split('.')[0]
    print(audio_filename)
    cat_label = audio_filename[5]
    with open(tmpdir+'emodb_cat_labels.csv', 'a') as f:
        f.write(cat_label+'\n')
       
    #extract eGeMAPS features for the utterance
    os.system('sh /Users/scuba/Softwares/bitbucket/audio_preprocessing/features/opensmile/eGEMAPs/egemaps_single.sh '+audio_file+' '+tmpdir+'feats.csv > /dev/null 2>&1') 
    os.system('cat '+tmpdir+'feats.csv | sed "s/;/,/g" >> '+tmpdir+'emodb_eGeMAPS.csv') 

outdir='features/emodb/'
os.system('cp -r '+tmpdir+'emodb* '+outdir)
os.system('rm smile.log')

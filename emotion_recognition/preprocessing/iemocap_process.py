import pdb, os, sys, glob
import numpy as np

datadir='/Volumes/WD_external_2TB/datasets/IEMOCAP/IEMOCAP_full_release/'
sess_dirs = glob.glob(datadir+'Session*')


tmpdir='/tmp/iemocap_process_tmp/'
os.system('mkdir -p '+tmpdir)
os.system('rm -r '+tmpdir+'*')
for sess_dir in sess_dirs:
    wav_dir = sess_dir+'/sentences/wav/'
    audio_dirs = glob.glob(wav_dir+'Ses*')
    for audio_dir in audio_dirs:
        audio_dir_id = audio_dir.split('/')[-1]
        emo_file = sess_dir+'/dialog/EmoEvaluation/'+audio_dir_id+'.txt'

        audio_files = glob.glob(audio_dir+'/*.wav')
        for audio_file in audio_files:
            audio_filename = audio_file.split('/')[-1].split('.')[0]
            with open(tmpdir+'IEMOCAP_audio_filenames.csv', 'a') as f:
                f.write(audio_filename+'\n') 
            continue

            print sess_dir.split('/')[-1],audio_dir_id,audio_filename
            os.system('cat '+emo_file+' | grep '+audio_filename+' > '+tmpdir+'extracted_line')
            with open(tmpdir+'extracted_line', 'r') as f:
                line = f.readlines()
            line_parts = line[0].split()  
            cat_label = line_parts[4]
            with open(tmpdir+'IEMOCAP_cat_labels.csv', 'a') as f:
                f.write(cat_label+'\n')
            cont_label = [float(line_parts[5][1:-1])]
            cont_label.append(float(line_parts[6][0:-1]))
            cont_label.append(float(line_parts[7][0:-1]))
            with open(tmpdir+'IEMOCAP_cont_labels.csv', 'a') as f:
                for i in range(0, len(cont_label)):
                    n = cont_label[i]
                    if i!=len(cont_label)-1:
                        f.write(str(n)+',')
                    else:
                        f.write(str(n)+'\n')  
               
            #extract eGeMAPS features for the utterance
            os.system('sh /Users/scuba/Softwares/bitbucket/audio_preprocessing/features/opensmile/eGEMAPs/egemaps_single.sh '+audio_file+' '+tmpdir+'feats.csv > /dev/null 2>&1') 
            os.system('cat '+tmpdir+'feats.csv | sed "s/;/,/g" >> '+tmpdir+'IEMOCAP_eGeMAPS.csv') 

sys.exit(0)
outdir='/Users/scuba/jati\@usc.edu/My\ Classes/CS534/project/2017_cs534_project_endtoendnegotiator/emotion_recognition/preprocessing/features/' 
os.system('cp -r '+tmpdir+'IEMOCAP* '+outdir)
os.system('rm smile.log')

import pdb
import numpy as np
import os
import sys
import scipy

from sklearn import preprocessing
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import KFold
from sklearn.feature_selection import f_classif
from sklearn.feature_selection import SelectKBest
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.feature_selection import VarianceThreshold
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import Imputer
from itertools import compress
import random
from time import gmtime, strftime

# Global variables
feat_dir = '../preprocessing/features/IEMOCAP/'
#emotions = ['neu', 'sad', 'ang', 'hap', 'fru', 'exc']
emotions = ['neu', 'sad', 'ang', 'hap']

is_emodb=False
if is_emodb:
    emodb_feat_dir='../preprocessing/features/emodb/'
    emodb_emotions = ['N', 'T', 'W', 'F'] #neutral, sad, angry, happy in German
    #write this in same order as "emotions"



def fAdaBoostLargeGridSearch(X_train_dev, y_train_dev, ids, t):
    params = [{ 'n_estimators': [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 120, 150, 180, 200, 250, 300],
                'learning_rate': [0.01, 0.05, 0.1, 0.5, 1.0, 2.0, 5.0] }]
    
    print("\nTuning hyper-parameters:")
    
    session_ids_train = []
    for _ in range(0,5):
        if _ != t:     
            session_ids_train.append(_)
    cv_indx = []
    dev_indx = []
    ids_train_dev = [x for i, x in enumerate(ids) if 'Ses0'+str(t+1) not in x]
    for sess_id in session_ids_train:
        #rand_id = random.choice(session_ids_train)
        train_indx_tmp, dev_indx_tmp = generate_cv_indx(ids_train_dev, 'Ses0'+str(sess_id+1))
        cv_indx.append((train_indx_tmp, dev_indx_tmp))

    #grid_search_cv = GridSearchCV(AdaBoostClassifier(DecisionTreeClassifier(class_weight='balanced')), 
                                    #params, cv=cv_indx, scoring='accuracy', refit=False, verbose=4, n_jobs=16)
    grid_search_cv = GridSearchCV(AdaBoostClassifier(DecisionTreeClassifier(max_depth=5, class_weight='balanced')), 
                                    params, cv=cv_indx, scoring='accuracy', refit=False, verbose=4, n_jobs=16)

    grid_search_cv.fit(X_train_dev, y_train_dev)

    print("Best parameters set found by cross validation:")
    print(grid_search_cv.best_params_)
    #test with the best parames
    #model=AdaBoostClassifier(DecisionTreeClassifier(class_weight='balanced'), learning_rate=grid_search_cv.best_params_['learning_rate'], n_estimators=grid_search_cv.best_params_['n_estimators'])
    model=AdaBoostClassifier(DecisionTreeClassifier(max_depth=5, class_weight='balanced'), learning_rate=grid_search_cv.best_params_['learning_rate'], n_estimators=grid_search_cv.best_params_['n_estimators'])

    model.fit(X_train_dev, y_train_dev)
    return model, grid_search_cv.best_params_['n_estimators'], grid_search_cv.best_params_['learning_rate']


def map_cat_labels_to_integer(labels, local_emotions):
    int_labels = []
    for l in labels: 
        int_labels.append(local_emotions.index(l))
    return int_labels

#load data
def load_data():
    if is_emodb:
        #load categorical labels
        with open(emodb_feat_dir+'emodb_cat_labels.csv', 'r') as f:
            emodb_cat_labels = f.read().splitlines()
        #load features
        X_emodb = np.genfromtxt(open(emodb_feat_dir+'emodb_eGeMAPS.csv', 'r'), delimiter=',')
        #emodb_sample_indx = boolean array indicating indices of only
        #those samples that have labels as defined in variable emotions
        emodb_sample_indx = np.zeros(len(emodb_cat_labels))
        i=0
        for emo in emodb_cat_labels:
            flag = False
            for emo_base in emodb_emotions:
                flag = flag or (emo==emo_base)
            emodb_sample_indx[i]=flag
            i+=1      
        emodb_sample_indx = emodb_sample_indx.astype( bool)

        X_emodb = X_emodb[emodb_sample_indx, :]
        y_str_emodb = list(compress(emodb_cat_labels, emodb_sample_indx))
        y_emodb = map_cat_labels_to_integer(y_str_emodb, emodb_emotions)


    #load filenames/ids
    with open(feat_dir+'IEMOCAP_audio_filenames.csv', 'r') as f:
        ids_all = f.read().splitlines()

    #load continuous labels
    #cont_labels  = np.genfromtxt(open(feat_dir+'IEMOCAP_cont_labels.csv', 'r'), delimiter=",",  missing_values=np.nan)

    #load categorical labels
    with open(feat_dir+'IEMOCAP_cat_labels.csv', 'r') as f:
        cat_labels = f.read().splitlines()

    #sample_indx = boolean array indicating indices of only
    #those samples that have labels as defined in variable emotions
    sample_indx = np.zeros(len(ids_all))
    i=0
    for emo in cat_labels:
        flag = False
        for emo_base in emotions:
            flag = flag or (emo==emo_base)
        sample_indx[i]=flag
        i+=1      
    sample_indx = sample_indx.astype( bool)
    
    #load features
    with open(feat_dir+'IEMOCAP_eGeMAPS.csv', 'r') as f:
        feats_str = f.read().splitlines()

    #this piece of code removes null feature string
    feats = np.zeros((len(feats_str), 88))
    for cnt in range(0, len(feats_str)):
        f = feats_str[cnt]
        if f=='':
            sample_indx[cnt] = False
            continue
        list_strs = f.split(',')
        list_nums = []
        for l in list_strs:
            list_nums.append(float(l))
        feats[cnt, :] = list_nums


    X = feats[sample_indx, :]
    y_str = list(compress(cat_labels, sample_indx))
    ids = list(compress(ids_all, sample_indx))
    y = map_cat_labels_to_integer(y_str, emotions)

    if is_emodb:
        return X,y,ids,X_emodb,y_emodb
    else:
        return X, y, ids
    

def feature_preprocess(X):
    imp = Imputer(missing_values='NaN', strategy='mean', axis=0)
    imp.fit(X)
    X=imp.transform(X)
    
    #remove features with zero variance
    zero_indices = VarianceThreshold()
    zero_indices.fit(X)
    X = zero_indices.transform(X)
    return X,imp,zero_indices

def generate_cv_indx(ids, string):
    train = [i for i, x in enumerate(ids) if string not in x] 
    test = [i for i, x in enumerate(ids) if string in x] 
    return train, test
    


def main():
    if is_emodb:
        outdir='outdir_cv_with_emodb_'+strftime("%Y_%m_%d_%H_%M_%S", gmtime())+'/'
    else:
        outdir='outdir_cv_'+strftime("%Y_%m_%d_%H_%M_%S", gmtime())+'/'
    try:
        os.mkdir(outdir)
    except OSError:
        os.system('rm -r '+outdir+'*')

    if is_emodb:
        X, y, ids, X_emodb, y_emodb = load_data()
    else:
        X, y, ids = load_data()

    X,imp,zero_indices = feature_preprocess(X)
    if is_emodb:    
        X_emodb = imp.transform(X_emodb)
        X_emodb = zero_indices.transform(X_emodb)

    scores=[]
    #leave on session out cross validation (LOSO)
    for t in range(0,5): #for 5 sessions
        string = 'Ses0'+str(t+1)
        train, test = generate_cv_indx(ids, string)


        X_train_dev = X[train,:]
        X_test = X[test,:]
        y_train_dev = [x for i, x in enumerate(y) if i in train]
        y_test = [x for i, x in enumerate(y) if i in test]

        if is_emodb:
            X_train_dev = np.vstack((X_train_dev, X_emodb))
            y_train_dev = np.concatenate((y_train_dev, y_emodb))

        #zscore normalization
        scaler = preprocessing.StandardScaler().fit(X_train_dev)
        X_train_dev = scaler.transform(X_train_dev)
        X_test = scaler.transform(X_test)
        
        #feature selection
        #select = SelectKBest(f_classif, k=50)
        #X_train_dev = select.fit_transform(X_train_dev, y_train_dev)
        #X_test = select.transform(X_test)
        
              
        #call method
        model, num_estimators, lr = fAdaBoostLargeGridSearch(X_train_dev, y_train_dev, ids, t)       

        #predict on test
        y_pred = model.predict(X_test)
        acc = accuracy_score(y_test, y_pred)
        scores.append(acc)
        print '\n\n===============================================\n'
        print 'accuracy in this fold = ', acc
        print '\n\n===============================================\n'
        np.savetxt(outdir+'scores.csv', scores)
        
    with open(outdir+'log.txt', 'w') as f:
        f.write('')
    with open(outdir+'log.txt', 'a') as f:
        f.write('\nAverage classification accuracy:\n')
        f.write('Accuracy = ' + str(np.mean(np.array(scores))) )

           

main()    

       

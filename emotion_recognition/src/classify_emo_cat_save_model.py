import pdb
import numpy as np
import os
import sys
import scipy

from sklearn import preprocessing
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import KFold
from sklearn.feature_selection import f_classif
from sklearn.feature_selection import SelectKBest
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.feature_selection import VarianceThreshold
from sklearn.metrics import accuracy_score
from itertools import compress
import random
from time import gmtime, strftime
from sklearn.externals import joblib


# Global variables
feat_dir = '../preprocessing/features/IEMOCAP/'
#emotions = ['neu', 'sad', 'ang', 'hap', 'fru', 'exc']
emotions = ['neu', 'sad', 'ang', 'hap']


def fAdaBoostLargeGridSearch(X_train_dev, y_train_dev, ids):
    params = [{ 'n_estimators': [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 120, 150, 180, 200, 250, 300],
                'learning_rate': [0.01, 0.05, 0.1, 0.5, 1.0, 2.0, 5.0] }]
    
    print("\nTuning hyper-parameters:")
    
    session_ids_train = [0,1,2,3,4]
    cv_indx = []
    dev_indx = []
    ids_train_dev = ids
    for sess_id in session_ids_train:
        #rand_id = random.choice(session_ids_train)
        train_indx_tmp, dev_indx_tmp = generate_cv_indx(ids_train_dev, 'Ses0'+str(sess_id+1))
        cv_indx.append((train_indx_tmp, dev_indx_tmp))

    grid_search_cv = GridSearchCV(AdaBoostClassifier(DecisionTreeClassifier(max_depth=5, class_weight='balanced')), 
                                    params, cv=cv_indx, scoring='accuracy', refit=False, verbose=4, n_jobs=16)
    grid_search_cv.fit(X_train_dev, y_train_dev)

    print("Best parameters set found by cross validation:")
    print(grid_search_cv.best_params_)
    #test with the best parames
    model=AdaBoostClassifier(DecisionTreeClassifier(max_depth=5, class_weight='balanced'),
                         learning_rate=grid_search_cv.best_params_['learning_rate'],
                         n_estimators=grid_search_cv.best_params_['n_estimators'])

    model.fit(X_train_dev, y_train_dev)
    return model, grid_search_cv.best_params_['n_estimators'], grid_search_cv.best_params_['learning_rate']


def map_cat_labels_to_integer(labels, emotions):
    int_labels = []
    for l in labels: 
        int_labels.append(emotions.index(l))
    return int_labels

#load data
def load_data():
    #load filenames/ids
    with open(feat_dir+'IEMOCAP_audio_filenames.csv', 'r') as f:
        ids_all = f.read().splitlines()

    #load continuous labels
    #cont_labels  = np.genfromtxt(open(feat_dir+'IEMOCAP_cont_labels.csv', 'r'), delimiter=",",  missing_values=np.nan)

    #load categorical labels
    with open(feat_dir+'IEMOCAP_cat_labels.csv', 'r') as f:
        cat_labels = f.read().splitlines()

    #sample_indx = boolean array indicating indices of only
    #those samples that have labels as defined in variable emotions
    sample_indx = np.zeros(len(ids_all))
    i=0
    for emo in cat_labels:
        flag = False
        for emo_base in emotions:
            flag = flag or (emo==emo_base)
        sample_indx[i]=flag
        i+=1      
    sample_indx = sample_indx.astype( bool)
    
    #load features
    with open(feat_dir+'IEMOCAP_eGeMAPS.csv', 'r') as f:
        feats_str = f.read().splitlines()

    #this piece of code removes null feature string
    feats = np.zeros((len(feats_str), 88))
    for cnt in range(0, len(feats_str)):
        f = feats_str[cnt]
        if f=='':
            sample_indx[cnt] = False
            continue
        list_strs = f.split(',')
        list_nums = []
        for l in list_strs:
            list_nums.append(float(l))
        feats[cnt, :] = list_nums


    X = feats[sample_indx, :]
    y_str = list(compress(cat_labels, sample_indx))
    ids = list(compress(ids_all, sample_indx))
    y = map_cat_labels_to_integer(y_str, emotions)

    return X, y, ids
    

def feature_preprocess(X):
    from sklearn.preprocessing import Imputer
    imp = Imputer(missing_values='NaN', strategy='mean', axis=0)
    X=imp.fit_transform(X)
    #remove features with zero variance
    zero_indices = VarianceThreshold()
    X = zero_indices.fit_transform(X)
    return X, imp, zero_indices

def generate_cv_indx(ids, string):
    train = [i for i, x in enumerate(ids) if string not in x] 
    test = [i for i, x in enumerate(ids) if string in x] 
    return train, test
    


def main():
    outdir='outdir_save_'+strftime("%Y_%m_%d_%H_%M_%S", gmtime())+'/'
    try:
        os.mkdir(outdir)
    except OSError:
        os.system('rm -r '+outdir+'*')

    X, y, ids = load_data()
    X, imp, zero_indices = feature_preprocess(X)

    #zscore normalization
    scaler = preprocessing.StandardScaler().fit(X)
    X = scaler.transform(X)
    
    #feature selection
    #select = SelectKBest(f_classif, k=50)
    #X_train_dev = select.fit_transform(X_train_dev, y_train_dev)
    #X_test = select.transform(X_test)
    
          
    #call method
    model, num_estimators, lr = fAdaBoostLargeGridSearch(X, y, ids)       

    #save model           
    joblib.dump(model, outdir+'model.pkl')
    joblib.dump(imp, outdir+'imputter.pkl')
    joblib.dump(zero_indices, outdir+'zero_indices.pkl')
    joblib.dump(scaler, outdir+'scaler.pkl')

main()    

       

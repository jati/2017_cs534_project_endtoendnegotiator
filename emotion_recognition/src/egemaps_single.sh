#! /bin/bash
audio_file=$1 #.wav file
output_file=$2 #name of the .csv file  
tmpdir=$3

os=`uname`
#echo $os
if [ "$os" != "Linux" ]
then
    #echo Not Linux
    #echo $os
    smile='emotion_recognition/openSMILE-2.2rc1/inst/bin/SMILExtract'
else
#    echo should be linux
#    echo $os
    smile='emotion_recognition/openSMILE-2.2rc1_linux/bin/linux_x64_standalone_static/SMILExtract'
fi
#printf '\n\n%s\n\n\n', $smile
config='emotion_recognition/openSMILE-2.2rc1/config/gemaps/eGeMAPSv01a.conf'
tmp_file=$output_file.tmp
$smile -C $config -I $audio_file -csvoutput $tmp_file 2> $tmpdir/log.txt
feature_values=`tail -1 $tmp_file | cut -d ';' -f 2-`
echo $feature_values > $output_file
rm $tmp_file



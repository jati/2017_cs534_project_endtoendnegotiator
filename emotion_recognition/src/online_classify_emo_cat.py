import pdb
import numpy as np
import os
import sys

from sklearn.externals import joblib
import warnings

# Global variables
#emotions = ['neu', 'sad', 'ang', 'hap', 'fru', 'exc']
emotions = ['neu', 'sad', 'ang', 'hap']

def main():

    inputs = list(sys.argv)
    file = inputs[1]
    x = np.genfromtxt(file, delimiter=';')

    #model_path='emotion_recognition/src/results/adaboost_lo_sess_o/outdir_2017_11_08_10_58_01/' #this one is 6-emotions model from IEMOCAP
    #model_path='emotion_recognition/src/results/adaboost_lo_sess_o/outdir_2017_11_21_17_44_20/' #this one is 4-emotions model from IEMOCAP

    #this is for debugging new models
    model_path='emotion_recognition/src/results/outdir_save_2017_11_22_22_21_47/'

    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        model = joblib.load(model_path+'model.pkl')    
        imp = joblib.load(model_path+'imputter.pkl')    
        scaler = joblib.load(model_path+'scaler.pkl')    
        zero_indices = joblib.load(model_path+'zero_indices.pkl')    

    x = x.reshape(1,-1)    

    x = imp.transform(x)
    x = zero_indices.transform(x)
    x = scaler.transform(x)
    
    #output = model.predict(x)
    #print("Emotion = ", emotions[output[0]])
    output = model.predict_proba(x)
    output=output[0]
    tmpdir=inputs[2]
    np.savetxt(tmpdir+'emotion.txt', output)
    # for i in range(0, len(emotions)):
    #     print(emotions[i], ':', round(output[i],4))

main()

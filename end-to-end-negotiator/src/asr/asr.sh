mkdir -p /tmp/
mkdir -p output/
sox -dq /tmp/recording.wav -V1 rate 8k channels 1 silence 1 0.1 3% 0 3.0 3%
printf "\nASR output:\n"
curl -T /tmp/recording.wav  "http://scuba@pano-03.usc.edu:8888/client/dynamic/recognize" -o /tmp/output.txt &> /dev/null
cat /tmp/output.txt | cut -d'"' -f8 | tee output/output.txt
printf "\n"

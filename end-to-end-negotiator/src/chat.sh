arg=$1
if [ "$arg" == "-t" ]
then
    input="text"
    affect_model="neutral"
elif [ "$arg" == "-n" ]
then
    input="voice"
    affect_model="neutral"
elif [ "$arg" == "-m" ]
then
    input="voice"
    affect_model="match"
elif [ "$arg" == "" ]
then
    input="voice"
    affect_model="laj"
else
    printf "Error please choose a valid command\n"
    printf " -t \t text input mode\n"
    printf " -n \t neutral output mode\n"
    printf " -m \t match affect mode\n"
    exit
fi

#deleting old asr_log file
rm asr_log/asr_log.txt 2>/dev/null

python3 spoken_chat.py --model_file trained_model.th --smart_ai --context_file data/negotiate/selfplay.txt --temperature 0.5 --ref_text data/negotiate/train.txt --input $input --affect_model $affect_model

from random import shuffle
from subprocess import call as bash
from functools import reduce
from webbrowser import open as urlopen
import glob

repsPerCond = 2

subjNum = 1
while len(glob.glob("testingData/log" + str(subjNum) + ".txt")):
    subjNum += 1

logFname = "log" + str(subjNum) + ".txt"
# -t is text input and voice output
# -n is vocal input and neutral output
# -m is vocal input and matching output
# no switch is vocal input and affective modeled response
conditions = [["script","-qa","testingData/" + logFname,"./chat.sh","-t"],["script","-qa","testingData/" + logFname,"./chat.sh","-n"],["script","-qa","testingData/" + logFname,"./chat.sh","-m"],["script","-qa","testingData/" + logFname,"./chat.sh"]] * repsPerCond
shuffle(conditions)

for command in conditions:
    with open('testingData/' + logFname,'a') as file:
        _ = file.write(reduce(lambda x,y: x + ' ' + y,command))
        _ = file.write('\n')
    bash(command)

urlopen('https://goo.gl/forms/bQPhLoi0syIdD9a03')

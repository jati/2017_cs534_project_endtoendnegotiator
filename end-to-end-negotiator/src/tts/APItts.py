# coding=utf-8
from watson_developer_cloud import TextToSpeechV1

text_to_speech = TextToSpeechV1(
    username='0a319f42-5490-4df0-81ed-2681e4b10126',
    password='Pun6R8mcLq6h',
    x_watson_learning_opt_out=True)  # Optional flag

with open('tts/testText.txt','r') as file:
    outText = file.read()

audio = text_to_speech.synthesize(outText, accept='audio/wav',voice="en-US_AllisonVoice")

with open('output.wav','wb') as audio_file:
    audio_file.write(audio)

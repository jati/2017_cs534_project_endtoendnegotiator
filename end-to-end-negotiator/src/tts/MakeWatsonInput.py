import sys
import os
import re
from random import random

def changeParam(voicexfm,paramDict):
    outxfm = voicexfm
    for param in paramDict.keys():
        value = int(re.findall(param+r"=\"(-?[0-9]{1,3})\%\"",voicexfm)[0])
        value += int(paramDict[param])
        value = max(-100,min(value,100)) #limiting the values to be between -100 and 100, the maximum range allowed
        outxfm = re.sub(param+r"=\"-?[0-9]{1,3}\%\"",param+r'="'+str(value)+'%"',outxfm)
    return outxfm

if len(sys.argv) < 4:
    stress = 'none'
else:
    stress = sys.argv[3].lower()
if len(sys.argv) < 3:
    emotion = input('Please enter an emotional state: ').lower()
else:
    emotion = sys.argv[2].lower()

print(3*'\n' + emotion + 3*'\n')

possibleEmos = ['pleading','happy','neutral','pushy','angry','sad']
while emotion not in possibleEmos:
    emotion = raw_input('Please select one of the following emotional states:\n' + reduce(lambda x,y: x.title() + ', ' +  y.title(),possibleEmos) + ': ').lower()

with open(sys.argv[1]) as file:
    text = file.read()

print(text)

outstring = ''
endxfm = '</voice-transformation>'

if emotion == 'pleading':
    voicexfm = '<voice-transformation type="Custom" breathiness="0%" glottal_tension="-100%" rate="10%" pitch_range="100%" pitch="100%"> '
elif emotion == 'happy':
    voicexfm = '<voice-transformation type="Custom" breathiness="100%" glottal_tension="-20%" rate="20%" pitch_range="80%" pitch="30%"> '
elif emotion == 'neutral':
    voicexfm = '<voice-transformation type="Custom" breathiness="0%" glottal_tension="10%" rate="30%" pitch_range="30%" pitch="-30%"> '
elif emotion == 'pushy':
    voicexfm = '<voice-transformation type="Custom" breathiness="0%" glottal_tension="30%" rate="50%" pitch_range="70%" pitch="-50%"> '
elif emotion == 'angry':
    voicexfm = '<voice-transformation type="Custom" breathiness="0%" glottal_tension="30%" rate="50%" pitch_range="-70%" pitch="-20%"> '
elif emotion == 'sad':
    voicexfm = '<voice-transformation type="Custom" breathiness="100%" glottal_tension="-30%" rate="-60%" pitch_range="-50%" pitch="-40%"> '

outstring += (voicexfm + text + endxfm)

if stress.lower() == 'object':
    emphList = ['hat','hats','ball','balls','book','books']
elif stress.lower() == 'subject':
    emphList = ['i','me','my','mine','myself',"i'm",'you','your','yours','yourself',"you're"]
elif stress.lower() == 'number':
    emphList = re.findall(r"([0-9]+)",outstring)
     # ['one','two','three','four','five','six','seven','eight','nine','ten','zero'] is there an easier way to do the identification of number words?
elif stress.lower() == 'verbal':
    emphList = ['need','have','want','give','trade'] #what other verbs does it use? I don't really want to use a parser to identify the verbs... also we don't necessarily want ALL verbs to be stressed
else:
    emphList = []

for word in emphList:
    # outstring = re.sub('\\b'+word+'\\b',' <emphasis> '+word+' </emphasis> ',outstring,flags=2) #the flags=2 kwarg means ignore case, this is a simpler case with just putting the built in emphasis tag on the syllable
    outstring = re.sub('\\b'+word+'\\b',endxfm+changeParam(voicexfm,{'rate':-60,'pitch':100})+'<emphasis> '+word+' </emphasis>'+endxfm+voicexfm,outstring,flags=2)

outstring = re.sub(r"\s[,.?!]\s",'',outstring)

print(3*'\n' + outstring + 3*'\n')



#stochastic emotional appending

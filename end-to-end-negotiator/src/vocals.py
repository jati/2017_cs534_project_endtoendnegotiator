# Copyright 2017-present, Maury Lander-Portnoy <landerpo@usc.edu>, Victor Ardulov <ardulov@usc.edu>, and Arindam Jati <jati@usc.edu>
# All rights reserved.
#
# This source code is licensed under the license found in the
# LICENSE file in the root directory of this source tree.

import pathlib
import glob
import os
from subprocess import call as bash
from subprocess import PIPE as devnull
from time import sleep
import sys
import re
import functools
from random import random as rand
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from string import punctuation

punc_table = str.maketrans('','',punctuation)

ibm_website = 'https://text-to-speech-demo.mybluemix.net/'
ssml_btn_xpath = '//*[@id="root"]/section/div/div[2]/ul/li[2]/a'
text_box_xpath = '//*[@id="root"]/section/div/div[2]/div/div/textarea'
download_btn_xpath = '//*[@id="root"]/section/div/div[3]/div[1]/div[1]/button[1]'

browser = None

if os.getcwd()[-25:] == 'end-to-end-negotiator/src': #cleaning the tmp folder at the beginning
    tmpFiles = glob.glob('tts/pastEmotion.txt')
    for fname in tmpFiles:
        os.remove(fname)

def randInd(distribution):
    if sum(distribution) != 1.: #normalizing the distribution if it's probability mass isn't 1
        normFactor = sum(distribution)
        distribution = list(map(lambda x: x/normFactor,distribution))
    bound = rand()
    for p,prob in enumerate(distribution):
        bound -= prob
        if bound < 0:
            return p

def cleanOutPuncs(out_going_text):
    puncList = []
    for i,item in enumerate(out_going_text):
        if item in punctuation:
            out_going_text[i-1] += item
            puncList.append(i)
    puncList.reverse()
    for index in puncList:
        del out_going_text[index]
    return out_going_text

def asr():
    _ = os.system('cd ../..;sh run.sh;cd - > /dev/null')
    fpathTxt = os.getcwd() + '/tmp/output.txt'
    while not len(glob.glob(fpathTxt)):
        sleep(.1)
    with open(fpathTxt,'r') as file:
        asrOut = file.read()
    os.remove(fpathTxt)
    return asrOut

def tts(words, emotion, stress='none'):
    global browser
    string = makeSSML(words, emotion, stress)

    if browser is None:
        options = webdriver.ChromeOptions()
        options.add_argument('headless')
        # options.add_argument("--window-size=100,100")
        browser = webdriver.Chrome(chrome_options=options)
        browser.command_executor._commands["send_command"] = ("POST",'/session/$sessionId/chromium/send_command')
        params = {'cmd': 'Page.setDownloadBehavior', 'params': {'behavior': 'allow', 'downloadPath':os.getcwd() + '/tts/'}}
        browser.execute("send_command", params)
        browser.get(ibm_website)
        ssml_btn = browser.find_element_by_xpath(ssml_btn_xpath)
        ssml_btn.click()

    text_box = browser.find_element_by_xpath(text_box_xpath)
    text_box.clear()
    text_box.send_keys(string)

    download_btn = browser.find_element_by_xpath(download_btn_xpath)
    download_btn.send_keys(Keys.ENTER)
    while not len(glob.glob('tts/transcript.mp3')):
        sleep(.1)
    # _ = os.system('play -q tts/transcript.mp3; rm tts/transcript.mp3;')
    _ = bash(["play","-q","tts/transcript.mp3"],stderr=devnull)
    _ = bash(["rm","tts/transcript.mp3"],stderr=devnull)


def vocal_input():
    txt = asr()
    # print(txt.startswith('No workers available'))
    if txt.startswith('No workers available'):
        print("System: Sorry we didn't hear that properly...")
        txt = input('SpokenHuman (type your response):')
    else:
        words = str.translate(txt, punc_table).split()

        if not('hat' in words or 'hats' in words)and\
                not('book' in words or 'books' in words)and\
                not('ball' in words or 'balls' in words)and\
                not('deal' in words or 'ok' in words or 'okay' in words or 'agree' in words) and\
                not('hello' in words or 'hi' in words or 'greetings' in words):
            print("System: Sorry we didn't hear that properly...")
            txt = input('SpokenHuman (type your response):')
    return txt

def text_input():
    txt = input('SpokenHuman (type your response):')
    return txt

def grabInEmos():
    while not len(glob.glob('tmp/emotion.txt')):
        sleep(.1)
    with open('tmp/emotion.txt','r') as file:
        text = file.read()
    emos = list(map(lambda x: float(x),text.splitlines()))
    return emos

def emo_model():

    emos = grabInEmos()

    if len(glob.glob('tts/pastEmotion.txt')):
        with open('tts/pastEmotion.txt','r') as file:
            oldText = file.read()
        pastEmos = list(map(lambda x: float(x),oldText.splitlines()))
    else:
        pastEmos = [0,0,0,0]

    affectiveState = list(map(lambda x,y: (.7*x) + y,pastEmos,emos))
    normFactor = sum(affectiveState)
    affectiveState = list(map(lambda x: x/normFactor,affectiveState)) #norming to sum to 1
    # _ = os.system('mv tmp/emotion.txt tts/pastEmotion.txt;')
    _ = bash(["mv","tmp/emotion.txt","tts/pastEmotion.txt"],stderr=devnull) #saving the current emotion for the next step

    outEmoProbs = []
    outInWeights = [[0.,-2.,1.,1.],[1.,1.,0.,-2.],[1.,0.,0.,0.],[.5,-2.5,1.,1.],[0.,-1.5,.5,1.]] #essentially captures our whiteboard graphic encoding solid lines as +/- 1 and dashed lines as .5, values are entirely tweakable, I also normalized the positive negative weights to sum to 1 except neutral so given an equal probability of all affective state classes, neutral will win
    for inWeights in outInWeights:
        outEmoProbs.append(functools.reduce(lambda x,y: x + y[0]*y[1],zip(inWeights,affectiveState),0))

    outEmoProbs = list(map(lambda x: max(x,0.),outEmoProbs)) #making sure the values are minimum of 0
    normFactor = sum(outEmoProbs)
    outEmoProbs = list(map(lambda x: x/normFactor,outEmoProbs)) #norming to sum to 1

    outEmoList = ['pleading','happy','neutral','angry','sad']
    responseEmo = outEmoList[randInd(outEmoProbs)]
    return responseEmo

def vocal_output(out_going_text):
    #ToDo: implement the spontaneous words appendix
    emotion = emo_model()
    out_going_text = cleanOutPuncs(out_going_text)
    tts(' '.join(out_going_text[:len(out_going_text) - 1]), emotion)
    # return out_going_tex

def neutral_output(out_going_text):
    out_going_text = cleanOutPuncs(out_going_text)
    tts(' '.join(out_going_text[:len(out_going_text) - 1]), 'neutral')

def match_output(out_going_text):
    emos = grabInEmos()
    inEmoList = ['neutral','sad','angry','happy']
    emotion = inEmoList[emos.index(max(emos))]
    out_going_text = cleanOutPuncs(out_going_text)
    tts(' '.join(out_going_text[:len(out_going_text) - 1]), emotion)

def makeSSML(text,emotion='neutral',stress='none'):
    def changeParam(voicexfm,paramDict):
        outxfm = voicexfm
        for param in paramDict.keys():
            value = int(re.findall(param+r"=\"(-?[0-9]{1,3})\%\"",voicexfm)[0])
            value += int(paramDict[param])
            value = max(-100,min(value,100)) #limiting the values to be between -100 and 100, the maximum range allowed
            outxfm = re.sub(param+r"=\"-?[0-9]{1,3}\%\"",param+r'="'+str(value)+'%"',outxfm)
        return outxfm

    if emotion not in ['pleading','happy','neutral','angry','sad']:
        emotion = 'neutral'

    if stress not in ['object','subject','number','verbal','none']:
        stress = 'none'


    outstring = ''
    endxfm = '</voice-transformation>'

    if emotion == 'pleading':
        voicexfm = '<voice-transformation type="Custom" breathiness="0%" glottal_tension="-100%" rate="10%" pitch_range="100%" pitch="100%"> '
    elif emotion == 'happy':
        voicexfm = '<voice-transformation type="Custom" breathiness="100%" glottal_tension="-20%" rate="20%" pitch_range="80%" pitch="30%"> '
    elif emotion == 'neutral':
        voicexfm = '<voice-transformation type="Custom" breathiness="0%" glottal_tension="10%" rate="30%" pitch_range="30%" pitch="-30%"> '
    elif emotion == 'angry':
        voicexfm = '<voice-transformation type="Custom" breathiness="0%" glottal_tension="30%" rate="50%" pitch_range="70%" pitch="-50%"> '
    elif emotion == 'sad':
        voicexfm = '<voice-transformation type="Custom" breathiness="100%" glottal_tension="-30%" rate="-60%" pitch_range="-50%" pitch="-40%"> '

    outstring += (voicexfm + text + endxfm)

    if stress.lower() == 'object':
        emphList = ['hat','hats','ball','balls','book','books']
    elif stress.lower() == 'subject':
        emphList = ['i','me','my','mine','myself',"i'm",'you','your','yours','yourself',"you're"]
    elif stress.lower() == 'number':
        emphList = re.findall(r"([0-9]+)",outstring)
         # ['one','two','three','four','five','six','seven','eight','nine','ten','zero'] is there an easier way to do the identification of number words?
    elif stress.lower() == 'verbal':
        emphList = ['need','have','want','give','trade'] #what other verbs does it use? I don't really want to use a parser to identify the verbs... also we don't necessarily want ALL verbs to be stressed
    else:
        emphList = []

    for word in emphList:
        # outstring = re.sub('\\b'+word+'\\b',' <emphasis> '+word+' </emphasis> ',outstring,flags=2) #the flags=2 kwarg means ignore case, this is a simpler case with just putting the built in emphasis tag on the syllable
        outstring = re.sub('\\b'+word+'\\b',endxfm+changeParam(voicexfm,{'rate':-60,'pitch':100})+'<emphasis> '+word+' </emphasis>'+endxfm+voicexfm,outstring,flags=2)

    outstring = re.sub(r"\s[,.?!]\s",'',outstring)

    #stochastic emotional appending

    return outstring

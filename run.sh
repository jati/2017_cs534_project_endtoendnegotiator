tmpdir=end-to-end-negotiator/src/tmp/
mkdir -p $tmpdir
rm -r $tmpdir/* 2>/dev/null

#run asr bash script
#the audio file is in asr/tmp/
cd asr/
chmod 777 record_utter.sh
./record_utter.sh ../$tmpdir
cd - > /dev/null
#extract eGeMAPS features for the utterance
feats=$tmpdir/feats.csv
audio=$tmpdir/recording2.wav
sh emotion_recognition/src/egemaps_single.sh $audio $feats $tmpdir
rm smile.log

#run emotion recognition module
python emotion_recognition/src/online_classify_emo_cat.py $feats $tmpdir

#run NLTK text emotion recognition module
TXT=`cat $tmpdir/output.txt`
curl -d "text=$TXT" http://text-processing.com/api/sentiment/ -o $tmpdir/emotion_text.txt 2> $tmpdir/log.txt
# printf "\n\nEmotion in text:\n"
# cat $tmpdir/emotion_text.txt
# printf "\n\n"
